g++ -O3 -std=c++0x utils.cpp sort_cpu.cpp -lboost_chrono -lboost_system -o sort_cpu_test
g++ -O3 -std=c++0x utils.cpp sort_cpu.cpp -lboost_chrono -lboost_system  -fopenmp -D_GLIBCXX_PARALLEL -o sort_cpu_test_mp
