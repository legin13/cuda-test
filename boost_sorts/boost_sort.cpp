#include <iostream>
#include <vector>
#include <random>
#include <boost/chrono.hpp>
#include <algorithm>
#include <thread>
#include <ctime>
#include "utils.h"
#include <boost/range/algorithm.hpp>


double run_tests(std::vector<double> &V, size_t parts, size_t mem)
{

    auto start = boost::chrono::steady_clock::now();

    boost::sort(V);

    auto end = boost::chrono::steady_clock::now();

    return boost::chrono::duration <double, boost::milli> (end - start).count();
}



int main(int argc, char **argv)
{
    std::vector<double> V;

    //use the system time to create a random seed
    unsigned int seed = (unsigned int) time(nullptr);

    size_t parts;

    if(argc != 2)
    {
        std::cout << "ERROR! Correct program usage:" << std::endl;
        std::cout << argv[0] << " nr_parts" << std::endl;
        std::exit(1);
    }

    // Get the number of parts
    std::string s(argv[1]);
    parts = (size_t) std::stoi(s);


    size_t mem = 16777216000;

    for(size_t i = 16; i <= mem; i *=4)
    {
        //Fill V with random numbers in the range [0,1]:
        V.resize(i);
        rnd_fill(V, 0.0, 1.0, seed);

        std::cout << i << "\t" << run_tests(V, parts, i) << std::endl;
        
    }

    return 0;
}

