#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <boost/chrono.hpp>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sort.h>

//Fill a vector with random numbers in the range [lower, upper]
void rnd_fill(thrust::host_vector<double> &V, const double lower, const double upper, const unsigned int seed)
{

    //Create a unique seed for the random number generator
    srand(time(NULL));

    size_t elem = V.size();
    for( size_t i = 0; i < elem; ++i)
    {
        V[i] = (double) rand() / (double) RAND_MAX;
    }
}

int main()
{

    thrust::host_vector<double> V;
    thrust::device_vector<double> d_V;

    //use the system time to create a random seed
    unsigned int seed = (unsigned int) time(NULL);


    size_t mem = 16777216;

    for(size_t i = 16; i <= mem; i *=4)
    {
        //Fill V with random numbers in the range [0,1]:
        V.resize(i);
        rnd_fill(V, 0.0, 1.0, seed);


        d_V = V; // Transfer data to the GPU
        
       

        boost::chrono::steady_clock::time_point start = boost::chrono::steady_clock::now();        
        thrust::sort(d_V.begin(), d_V.end());
	boost::chrono::steady_clock::time_point end = boost::chrono::steady_clock::now();

       
        V = d_V; // Transfer data to the CPU

         double dt1 = boost::chrono::duration <double, boost::milli> (end - start).count();


        //std::cout << i << "\t" << elapsedTime << "\t" << dt1 + dt2 << std::endl;
        std::cout << i << "\t" <<  dt1<< std::endl;

    }

    return 0;
}

