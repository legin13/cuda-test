#
# Include file with basic definitions for all examples for Linux
#
# Author: Alex V. Boreskoff
#

SHELL          = /bin/sh
GLUTPATH       = /usr/include/GL
LIBEXTPATH     = ../libExt
LIBTEXTUREPATH = ../libTexture
LIB3DPATH      = ../3D
UTILSPATH      = ../utils
PBUFFERPATH    = ../PBuffer/Linux
PROGRAMPATH    = ../Program
CAMERAPATH     = ../libCamera
LIBMESHPATH    = ../libMesh
FRAMEBUFFERPATH = ../Framebuffer
UNRARPATH       = ../unrarlib/unrarlib

PBUFFERINC      = -I../PBuffer -I../PBuffer/Linux
CFLAGS          = -x c++ -g -I/usr/include/GL -Wall -I$(GLUTPATH)/include/GL
 
CONLYFLAGS      = -g -I/usr/include/GL -Wall -I$(GLUTPATH)/include/GL
 
LIBS            = -L/usr/X11R6/lib -L$(GLUTPATH)/lib -lglut -lGL -lGLU  -lXt -lX11 -lm -ljpeg -lz -lpng

LIBEXT_OBJS     = libExt.o
LIBTEXTURE_OBJS = libTexture.o Texture.o CompressedTexture.o BmpLoader.o TgaLoader.o DdsLoader.o JpegLoader.o PngLoader.o Data.o ZipFileSystem.o FileSystem.o unrarlib.o RarFileSystem.o
OBJS            = $(LIBEXT_OBJS) $(LIBTEXTURE_OBJS) Vector2D.o Vector3D.o Vector4D.o 
INCLUDE         = -I$(LIBEXTPATH) -I$(LIBTEXTUREPATH) -I$(LIB3DPATH) -I$(UTILSPATH) -I$(PROGRAMPATH) -I$(PBUFFERINC) -I$(CAMERAPATH) -I$(LIBMESHPATH) -I$(FRAMEBUFFERPATH) -I$(UNRARPATH)
