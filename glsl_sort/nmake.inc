#
# Include file with basic definitions for all examples for Win32
#
# Author: Alex V. Boreskoff
#

# flags for debug mode (for compile and link)

DEBUG = 1

!if defined (DEBUG)
DEBUG_COMPILE_FLAGS = /Zi /MT
DEBUG_LINK_FLAGS    = /DEBUG /DEBUGTYPE:CV /NODEFAULTLIB:libc.lib /NODEFAULTLIB:msvcrt.lib /NODEFAULTLIB:msvcprt.lib libcmt.lib 
!else
DEBUG_COMPILE_FLAGS = /MT
DEBUG_LINK_FLAGS    = /NODEFAULTLIB:libc.lib /NODEFAULTLIB:msvcrt.lib /NODEFAULTLIB:msvcprt.lib libcmt.lib 

!endif

CC              = cl
LINK            = link
OPENGL          = glut32.lib glu32.lib opengl32.lib
GLUTPATH        = ../glut
LIBEXTPATH      = ../libExt/
LIBTEXTUREPATH  = ../libTexture/
LIB3DPATH       = ../3D/
UTILSPATH       = ../utils
PBUFFERPATH     = ../PBuffer/Win32
PROGRAMPATH     = ../Program
CAMERAPATH      = ../libCamera
LIBMESHPATH     = ../libMesh
FRAMEBUFFERPATH = ../Framebuffer
UNRARPATH       = ../unrarlib/unrarlib
OPENEXRPATH     = ../Openexr-1.4.0

PBUFFERINC      = -I../PBuffer -I../PBuffer/Win32
INC             = $(INC) -I$(GLUTPATH) -I$(LIBEXTPATH) -I$(LIBTEXTUREPATH) -I$(UTILSPATH) -I$(LIB3DPATH) -I$(PROGRAMPATH) -I$(LIBTEXTUREPATH)zlib -I$(LIBTEXTUREPATH)libpng -I$(LIBTEXTUREPATH)jpeg -I$(CAMERAPATH) -I$(LIBMESHPATH) -I$(FRAMEBUFFERPATH) -I$(UNRARPATH) #-I$(OPENEXRPATH)\Imath -I$(OPENEXRPATH)\IlmImf -I$(OPENEXRPATH)\Half -I$(OPENEXRPATH)\Iex
LLDLIBS         = $(LFLAGS) $(DEBUG_LINK_FLAGS) $(OPENGL) $(guilibs) $(LIBTEXTUREPATH)jpeg/libjpeg.lib $(LIBTEXTUREPATH)libpng/libpng.lib $(LIBTEXTUREPATH)zlib/zlib.lib /LIBPATH:$(GLUTPATH) #/LIBPATH:$(OPENEXRPATH)/vc/vc8/lib Iex.lib IlmThread.lib Imath.lib IlmImf.lib  Half.lib IlmThread.lib 
LIBEXT_OBJS     = libExt.obj
LIBTEXTURE_OBJS = libTexture.obj Texture.obj CompressedTexture.obj BmpLoader.obj TgaLoader.obj DdsLoader.obj JpegLoader.obj PngLoader.obj Data.obj ZipFileSystem.obj FileSystem.obj unrarlib.obj RarFileSystem.obj #ExrLoader.obj
OBJS            = $(LIBEXT_OBJS) $(LIBTEXTURE_OBJS) Vector3D.obj Vector2D.obj
CFLAGS          = $(CFLAGS) /EHsc $(DEBUG_COMPILE_FLAGS) -DWIN32 -D_CRT_SECURE_NO_DEPRECATE

