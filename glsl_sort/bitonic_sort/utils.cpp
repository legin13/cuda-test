//
// Simple utilities
//

#include	"libExt.h"
#include <vector>
#include <random>

void	drawQuad ( int w, int h )
{
    glEnable      ( GL_TEXTURE_2D );
    glBegin       ( GL_QUADS );
    glTexCoord2f ( 0, 0 );
    glVertex2f   ( 0, 0 );

    glTexCoord2f ( 1, 0 );
    glVertex2f   ( w, 0 );

    glTexCoord2f ( 1, 1 );
    glVertex2f   ( w, h );

    glTexCoord2f ( 0, 1 );
    glVertex2f   ( 0, h );
    glEnd   ();
}

void    startOrtho ( int w, int h )
{
    glMatrixMode   ( GL_PROJECTION );                   // select the projection matrix
    glPushMatrix   ();                                  // store the projection matrix
    glLoadIdentity ();                                  // reset the projection matrix
    // set up an ortho screen
    glOrtho        ( 0, w, 0, h, -1, 1 );
    glMatrixMode   ( GL_MODELVIEW );                    // select the modelview matrix
    glPushMatrix   ();                                  // store the modelview matrix
    glLoadIdentity ();                                  // reset the modelview matrix

    glDisable   ( GL_DEPTH_TEST );
    glDepthMask ( GL_FALSE );
}

void    endOrtho ()
{
    glMatrixMode ( GL_PROJECTION );                     // select the projection matrix
    glPopMatrix  ();                                    // restore the old projection matrix
    glMatrixMode ( GL_MODELVIEW );                      // select the modelview matrix
    glPopMatrix  ();                                    // restore the old projection matrix

    glEnable    ( GL_DEPTH_TEST );
    glDepthMask ( GL_TRUE );
}

void init ()
{
    glClearColor ( 0, 0, 0, 1 );
    glEnable     ( GL_DEPTH_TEST );
    glDepthFunc  ( GL_LEQUAL );

    glHint ( GL_POLYGON_SMOOTH_HINT,         GL_NICEST );
    glHint ( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
}


//Fill a vector with random numbers in the range [lower, upper]
void rnd_fill(std::vector<double> &V,  double lower,  double upper,  unsigned int seed)
{

    //use the default random engine and an uniform distribution
    std::default_random_engine eng(seed);
    std::uniform_real_distribution<double> distr(lower, upper);

    for( auto &elem : V)
    {
        elem = distr(eng);
    }
}

