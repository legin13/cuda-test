//
// Return floating-point texture format with given precision
//

#include	"libExt.h"

GLenum	fpFormatWithPrecision ( int bits )
{
#ifdef	MACOSX
	if ( isExtensionSupported ( "GL_APPLE_float_pixels" ) )
		return (bits == 16 ? GL_RGBA_FLOAT16_APPLE : GL_RGBA_FLOAT32_APPLE);
	else
		return 0;
#else
	if ( isExtensionSupported ( "GL_ARB_texture_float" ) )
		return (bits == 16 ? GL_RGBA16F_ARB : GL_RGBA32F_ARB);
	else
	if ( isExtensionSupported ( "GL_NV_float_buffer" ) )
		return (bits == 16 ? GL_FLOAT_RGBA16_NV : GL_FLOAT_RGBA32_NV );
	else
	if ( isExtensionSupported ( "GL_ATI_texture_float" ) )
		return (bits == 16 ? GL_RGBA_FLOAT16_ATI : GL_RGBA_FLOAT32_ATI);
#endif
	return 0;
}
