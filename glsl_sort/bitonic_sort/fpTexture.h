
#ifndef	__FP_TEXTURE__
#define	__FP_TEXTURE__

#include	"libExt.h"

GLenum	fpFormatWithPrecision ( int bits = 32 );

#endif
