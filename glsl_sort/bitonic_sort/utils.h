//
// Simple utilities
//
#include <vector>
#include <random>
void	drawQuad   ( int w, int h );
void    startOrtho ( int w, int h );
void    endOrtho   ();
void	init       ();

//Fill a vector with random numbers in the range [lower, upper]
void rnd_fill(std::vector<double> &V,  double lower,  double upper,  unsigned int seed);

