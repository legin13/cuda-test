//
// Fragment shader for performing hafl-cleaner operation on source map for bitonic sort
//
// Author: Alex V. Boreskoff, <steps3d@narod.ru>
//

uniform	sampler2DRect	srcMap;
uniform	float			step;
uniform	int			dsize;
uniform int blockNum;
uniform int sbSize;
void	main ()
{
    int blockSize =  dsize * dsize/blockNum ;

    float	first    = 1.0;	// first (-1)/second (1)
    float	dir      = 1.0;// ascending (1)/descending (-1)
    vec2	texCur   = gl_TexCoord [0].xy;
    int real_pos = (int(gl_TexCoord [0].x)  + int(gl_TexCoord [0].y)* dsize);
    int dir_var = real_pos/blockSize;
    if (mod(dir_var,2) == 0.0)
    {
        dir = -1.0;
    }
    if (blockNum==-1)//special mode for lastStage
    {
        dir = -1.0;
    }
    int first_var = 2 * real_pos/sbSize;
    if (mod(first_var,2)==0.0)
    {
        first = -1.0;
    }
    vec2 texOther = texCur - sign ( first ) * vec2 ( step, 0.0 ); //it could be rewritten in normal coordinates of course.
    while (texOther[0]>float(dsize))
    {
        texOther[0] -= float(dsize);
        texOther[1] += 1.0;
    }
    while (texOther[0]<0.0)
    {
        texOther[0] += float(dsize);
        texOther[1] -= 1.0;
    }
    vec4	cur      = texture2DRect ( srcMap, texCur );
    vec4	other    = texture2DRect ( srcMap, texOther );
    float	key      = first * ( cur.a - other.a );



    if ( dir > 0.0 )								// we want in first min (), in second -> max
    {
        if ( key < 0.0 )
            cur = other;
    }
    else											// we want v1 > v2
    {
        if ( key > 0.0 )
            cur = other;
    }

    gl_FragColor = cur	;
    //cur;
}
