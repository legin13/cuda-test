//
// Example of using bitonic sort to sort 1D texture of RGBA values
//
// Author: Alex V. Boreskoff, <steps3d@narod.ru>
//
//

#include	"libExt.h"
#include	"libTexture.h"
#include	"fpTexture.h"
#include	"FrameBuffer.h"
#include	"GlslProgram.h"
#include	"utils.h"
#include <math.h>
#include <vector>
#include <GL/gl.h>
#include	<glut.h>
#include <boost/chrono.hpp>

#include	<stdio.h>
#include	<stdlib.h>


GLuint		dataMap;
GLenum		mapFormat;
GlslProgram	prog;

int SIZE;
int dSIZE;
FrameBuffer	* buf [2];
float		 * res;
int			  totalPasses = 0;

int	intLog ( int value )
{
    int	i = 0;

    for ( int n = value; n; n >>=1, i++ )
        ;

    i--;

    if ( value != (1 << i) )
        printf ( "Not a power of 2.\n" );

    return i;
}

bool	isPowerOfTwo ( int n )
{
    return (n & (n-1)) == 0;
}

void	printArray ( const char * title, float * buf )
{
    printf ( "\na [%d] = ", SIZE );

    for ( int i = 0; i < SIZE; i++ )
        printf ( "%f ", buf [4*i + 3] );

    printf ( "\n" );
}

void	createDataMap ()
{

    //printf ( "Creating dataMap\n" );
    std::vector<double> host_vector;

    host_vector.resize(SIZE);
    unsigned int seed = (unsigned int) time(NULL);

    rnd_fill(host_vector, 0.0, 1.0, seed);

    //float	values [4*SIZE];
    float * values = (float *) malloc(4*SIZE * sizeof(float));

    for ( long i = 0; i < SIZE; i++ )
    {
        values [4*i + 0] = 0;
        values [4*i + 1] = 0;
        values [4*i + 2] = 0;
        values [4*i + 3] = host_vector[i];//( i & 1 ? 15 - 2*i : 8 - i );
    }


    //printArray ( "Source", values );

    glGenTextures   ( 1, &dataMap );
    glBindTexture   ( GL_TEXTURE_RECTANGLE_ARB, dataMap );
    glPixelStorei   ( GL_UNPACK_ALIGNMENT, 1 );                         // set 1-byte alignment

    glTexParameteri ( GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP );    // set texture to repeat mode
    glTexParameteri ( GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP );
    glTexParameteri ( GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
    glTexParameteri ( GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

    glTexImage2D    ( GL_TEXTURE_RECTANGLE_ARB, 0, mapFormat, dSIZE, dSIZE, 0, GL_RGBA, GL_FLOAT, values );
    glBindTexture   ( GL_TEXTURE_RECTANGLE_ARB, 0 );
}

void	doStage ( int stage, int& index )
{
    int	numPasses    = stage;
    int	blockSize    = 1 << stage;
    int	numBlocks    = SIZE / blockSize;
    int	step         = blockSize / 2;
    int	subBlockSize = blockSize;
    int	numSubBlocks = 1;

    for ( int pass = 1; pass <= numPasses; pass++, step >>= 1, index ^= 1, totalPasses++ )
    {

        glActiveTextureARB ( GL_TEXTURE0_ARB );

        if ( stage == 1 )
            glBindTexture ( GL_TEXTURE_RECTANGLE_ARB, dataMap );
        else
            glBindTexture ( GL_TEXTURE_RECTANGLE_ARB, buf [index] -> getColorBuffer () );

        buf [index ^ 1] -> bind ();
        prog.bind ();
        prog.setUniformFloat ( "step", step );
        prog.setUniformInt ( "blockNum", numBlocks );
        prog.setUniformInt ( "sbSize", subBlockSize );
        prog.setUniformInt ( "dsize", dSIZE );
        startOrtho ( dSIZE, dSIZE );

        // now draw blocks
        glBegin ( GL_QUADS );
        glMultiTexCoord2f ( GL_TEXTURE0_ARB, 0 ,0 );
        glVertex2f           ( 0, 0 );
        glMultiTexCoord2f ( GL_TEXTURE0_ARB, 0,dSIZE);
        glVertex2f           ( 0, dSIZE );
        glMultiTexCoord2f ( GL_TEXTURE0_ARB, dSIZE,dSIZE);

        glVertex2f           ( dSIZE, dSIZE );
        glMultiTexCoord2f ( GL_TEXTURE0_ARB, dSIZE,0 );
        glVertex2f           ( dSIZE, 0);

        glEnd   ();
        glFlush ();

        endOrtho ();

        prog.unbind ();

        glBindTexture ( GL_TEXTURE_RECTANGLE_ARB, buf [index ^ 1] -> getColorBuffer () );

        glReadPixels  ( 0, 0, dSIZE, dSIZE, GL_RGBA, GL_FLOAT, res );

        buf [index ^ 1] -> unbind ();

        //printf     ( "stage %3d, blocksize %3d, pass %3d\n", stage, blockSize,pass );
        //printArray ( "", res );

        numSubBlocks *= 2;
        subBlockSize /= 2;

    }
}


void	doLastStage ( int stage, int& index )
{
    int	numPasses    = stage;
    int	blockSize    = 1 << stage;
    int	step         = blockSize / 2;
    int	subBlockSize = blockSize;
    int	numSubBlocks = 1;

    for ( int pass = 1; pass <= numPasses; pass++, step >>= 1, index ^= 1, totalPasses++ )
    {

        glActiveTextureARB ( GL_TEXTURE0_ARB );

        if ( stage == 1 )
            glBindTexture ( GL_TEXTURE_RECTANGLE_ARB, dataMap );
        else
            glBindTexture ( GL_TEXTURE_RECTANGLE_ARB, buf [index] -> getColorBuffer () );

        buf [index ^ 1] -> bind ();
        prog.bind ();
        prog.setUniformFloat ( "step", step );
        prog.setUniformInt ( "blockNum", -1 );
        prog.setUniformInt ( "sbSize", subBlockSize );
        prog.setUniformInt ( "dsize", dSIZE );

        startOrtho ( dSIZE, dSIZE );

        // now draw blocks
        glBegin ( GL_QUADS );

        glMultiTexCoord2f ( GL_TEXTURE0_ARB, 0 ,0 );
        glVertex2f           ( 0, 0 );
        glMultiTexCoord2f ( GL_TEXTURE0_ARB, 0,dSIZE  );
        glVertex2f           ( 0, dSIZE );
        glMultiTexCoord2f ( GL_TEXTURE0_ARB, dSIZE,dSIZE );

        glVertex2f           ( dSIZE, dSIZE );
        glMultiTexCoord2f ( GL_TEXTURE0_ARB, dSIZE,0 );
        glVertex2f           ( dSIZE, 0);



        glEnd   ();
        glFlush ();

        endOrtho ();

        prog.unbind ();

        glBindTexture ( GL_TEXTURE_RECTANGLE_ARB, buf [index ^ 1] -> getColorBuffer () );
        glReadPixels  ( 0, 0, dSIZE, dSIZE, GL_RGBA, GL_FLOAT, res );

        buf [index ^ 1] -> unbind ();

        //printf     ( "stage %3d, pass %3d\n", stage, pass );
        //printArray ( "", res );

        numSubBlocks *= 2;
        subBlockSize /= 2;
    }
}

void	doBitonicSort ()
{

    startOrtho ( SIZE, 1 );

    int	numStages = intLog ( SIZE );
    int	index     = 0;
    int	stage;

    glEnable      ( GL_TEXTURE_RECTANGLE_ARB );

    // do stages with comparing order changing
    for ( stage = 1; stage < numStages; stage++ )
        doStage ( stage, index );

    // do last stage with a fixed order
    doLastStage ( stage, index );

    endOrtho ();
}

void	printResults ()
{
    printf ( "----------------------\nTotal passes: %d\n", totalPasses );
    printArray ( "", res );
}

void	display ()
{

    createDataMap ();

    auto start = boost::chrono::steady_clock::now();
    doBitonicSort ();
    auto end = boost::chrono::steady_clock::now();

    printf("%lld\t%f\n",SIZE, boost::chrono::duration <double, boost::milli> (end - start).count());
    //printResults  ();

    exit ( 1 );
}

void	reshape ( int w, int h )
{
    glViewport ( 0, 0, w, h );
}

int main ( int argc, char * argv [] )
{
    SIZE = atoi(argv[1]);
    //ONLY FOR 2^n
    dSIZE = round(sqrt(SIZE));
    res = new float[4 * SIZE];


    // initialize glut
    glutInit            ( &argc, argv );
    glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize  ( 512, 512 );

    // create window
    glutCreateWindow ( "GPU Sorting" );

    // register handlers
    glutDisplayFunc  ( display );
    glutReshapeFunc  ( reshape );

    init                      ();
    initExtensions            ();
    assertExtensionsSupported ( "ARB_texture_rectangle" );

    if ( !FrameBuffer :: isSupported () )
    {
        printf ( "Fbo not supported\n" );

        return 1;
    }

    if ( !GlslProgram :: isSupported () )
    {
        printf ( "GLSL not supported\n" );

        return 1;
    }

    mapFormat = fpFormatWithPrecision ( 32 );

    if ( mapFormat == 0 )
    {
        printf ( "No fp format\n" );

        return 1;
    }

    for ( int i = 0; i < 2; i++ )
    {

        buf [i] = new FrameBuffer ( dSIZE, dSIZE );

        unsigned tempMap = buf [i] -> createColorRectTexture ( GL_RGBA, mapFormat );

        buf [i] -> create ();
        buf [i] -> bind   ();

        if ( !buf [i] -> attachColorTexture ( GL_TEXTURE_RECTANGLE_ARB, tempMap ) )
            printf ( "buffer error with color attachment\n");

        if ( !buf [i	] -> isOk () )
            printf ( "Error with framebuffer\n" );

        buf [i] -> unbind ();
    }

    if ( !prog.loadShaders ( "sort1.vsh", "sort1.fsh" ) )
    {
        printf ( "Error loading sort1 shaders:\n%s\n", prog.getLog ().c_str () );

        return 3;
    }


    glutMainLoop ();

    return 0;
}
