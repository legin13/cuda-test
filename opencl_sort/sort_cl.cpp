#include <vector>
#include <algorithm>
#include <boost/compute.hpp>
#include "utils.h"
#include <boost/chrono.hpp>
namespace compute = boost::compute;



int main()
{
    //use the system time to create a random seed
    unsigned int seed = (unsigned int) time(NULL);

    size_t mem = 16777216;

    // generate random numbers on the host
    std::vector<double> host_vector;

// get the default compute device
    compute::device gpu = compute::system::default_device();

    // create a compute context and command queue
    compute::context ctx(gpu);
    compute::command_queue queue(ctx, gpu);
    compute::vector<double> device_vector(1, ctx);
    for(size_t i = 16; i <= mem; i *=4)
    {

        //Fill V with random numbers in the range [0,1]:
        host_vector.resize(i);
        device_vector.resize(i);
        rnd_fill(host_vector, 0.0, 1.0, seed);




        // copy data to the device
        compute::copy(
            host_vector.begin(), host_vector.end(), device_vector.begin(), queue
        );
        auto start = boost::chrono::steady_clock::now();
        // sort data on the device
        compute::sort(
            device_vector.begin(), device_vector.end(), queue
        );
        auto end = boost::chrono::steady_clock::now();
        // copy data back to the host
        compute::copy(
            device_vector.begin(), device_vector.end(), host_vector.begin(), queue
        );
        std::cout << i<<'\t'<< boost::chrono::duration <double, boost::milli> (end - start).count() << std::endl;
        //std::cout << host_vector[0]<<'\n'<<host_vector[1]<<'\n'<<host_vector[2];
    }
}
