# -*- coding: utf-8 -*-
__author__ = 'bahteev'
from matplotlib import pyplot as plt
from matplotlib import rc
font = {'family':  "DejaVu Sans",
        'weight': 'normal',
        'size': 20}

rc('font', **font)

X = [4**i for i in range(2,13)]
Xl = [i for i in range(2,13)]
def report2array(fname):
	result = []
	with open(fname) as inp:
		for line in inp:			
			result.append(float(line.split('\t')[1]))
	return result 

Y_c1 = report2array('report_cpu1.txt')
Y_c2 = report2array('report_cpu2.txt')
Y_cuda = report2array('report_cuda.txt')
Y_cl = report2array('report_cl.txt')
Y_glsl = report2array('report_gl.txt')
Y_glsl_n = report2array('report_gl_nvidia.txt')
Y_omp = report2array('report_omp.txt')
Y_ompt = report2array('report_thrust_omp.txt')

plt.plot(X, Y_c1, label = u'CPU, 1 ядро')
plt.plot(X, Y_c2, label = u'CPU, 2 ядра')
plt.plot(X, Y_cuda, label = u'CUDA')
plt.plot(X, Y_cl, label = u'OpenCL')
plt.plot(X, Y_glsl, label = u'GLSL, Intel')
plt.plot(X, Y_glsl_n, label = u'GLSL, NVIDIA')
plt.plot(X, Y_omp, label = u'std::sort + OpenMP ')
plt.plot(X, Y_ompt, label = u'thrust::sort + OpenMP')

plt.ylim(0,1000)
plt.xlabel(u'количество элементов, n')
plt.ylabel(u'Время, мс')
plt.legend(loc='lower right')
plt.show()

plt.plot(Xl, Y_c1, label = u'CPU, 1 ядро')
plt.plot(Xl, Y_c2, label = u'CPU, 2 ядра')
plt.plot(Xl, Y_cuda, label = u'CUDA')
plt.plot(Xl, Y_cl, label = u'OpenCL')
plt.plot(Xl, Y_glsl, label = u'GLSL, Intel')
plt.plot(Xl, Y_glsl_n, label = u'GLSL, NVIDIA')
plt.plot(Xl, Y_omp, label = u'std::sort + OpenMP ')
plt.plot(Xl, Y_ompt, label = u'thrust::sort + OpenMP')
plt.ylim(0,1000)
plt.xlabel(u'log(n)')
plt.ylabel(u'Время, мс')
plt.legend(loc='upper left')
plt.show()
